﻿using Prueba.Application.DTO;
using Prueba.Application.Models;
using Prueba.Application.Profile;
using Prueba.Application.UseCases.Interface;
using Prueba.Domain.Empresa.Services;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using System;
using System.Collections.Generic;
using System.Text;
using Prueba.Infrastructure.Repositories;

namespace Prueba.Application.UseCases
{
    public class EmpresaUseCase : IEmpresaUseCase
    {
        private readonly IMapper _mapper;

        public EmpresaUseCase()
        {
            var mapConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ApplicationProfile>();
                cfg.AddExpressionMapping();
            });
            this._mapper = new Mapper(mapConfig);
        }

        public Response<ICollection<EmpresaDTO>> ListarEmpresas()
        {
            var response = new Response<ICollection<EmpresaDTO>>();

            try
            {
                using (var empresaRepository = new EmpresaRepository())
                {
                    response.Data = _mapper.Map<ICollection<EmpresaDTO>>(new EmpresaService(empresaRepository).ListarEmpresas());
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
                response.Succeeded = false;
            }

            return response;
        }
    }
}
