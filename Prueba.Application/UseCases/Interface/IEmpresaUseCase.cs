﻿using Prueba.Application.DTO;
using Prueba.Application.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Application.UseCases.Interface
{
    public interface IEmpresaUseCase
    {
        Response<ICollection<EmpresaDTO>> ListarEmpresas();
    }

}
