﻿using Prueba.Application.DTO;
using Prueba.Domain.Empresa.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Application.Profile
{
    public class ApplicationProfile :AutoMapper.Profile 
    {
        public ApplicationProfile()
        {
            CreateMap<EmpresaDTO, EmpresaEntity>().ReverseMap();
        }
    }
}
