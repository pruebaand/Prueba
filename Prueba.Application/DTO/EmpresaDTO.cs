﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Application.DTO
{
    public class EmpresaDTO
    {
        public int Nit { get; set; }

        public string Nombre { get; set; } = string.Empty;

        public string Correo { get; set; } = string.Empty;

        public string Celular { get; set; } = string.Empty;

        public string Direccion { get; set; } = string.Empty;

        public DateTime FechaFundacion { get; set; }

        public Boolean Estado { get; set; }
    }
}
