﻿using Prueba.Domain.Empresa.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Domain.Empresa.IRepositories
{
    public interface IEmpresaRepository
    {
        ICollection<EmpresaEntity> ListarEmpresas();
    }
}
