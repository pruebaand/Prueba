﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Domain.Empresa.Entities
{
    public class EmpresaEntity
    {
        public int Nit { get; set; }

        public string Nombre { get; set; } = string.Empty;

        public string Correo { get; set; } = string.Empty;

        public string Telefono { get; set; } = string.Empty;

        public string Direccion { get; set; } = string.Empty;

        public DateTime FechaFundacion { get; set; }

        public Boolean Estado { get; set; }
    }
}
