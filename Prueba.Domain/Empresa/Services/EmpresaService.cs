﻿using Prueba.Domain.Empresa.Entities;
using Prueba.Domain.Empresa.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Domain.Empresa.Services
{
    public class EmpresaService
    {
        private readonly IEmpresaRepository _empresaRepository;

        public EmpresaService(IEmpresaRepository personaRepository)
        {
            _empresaRepository = personaRepository;
        }
        public ICollection<EmpresaEntity> ListarEmpresas()
        {
            try
            {
                return _empresaRepository.ListarEmpresas();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
