﻿using Prueba.Domain.Empresa.Entities;
using Prueba.Domain.Empresa.IRepositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Prueba.Infrastructure.Repositories
{
    public class EmpresaRepository : IEmpresaRepository, IDisposable
    {
        public void Dispose()
        {          
        }

        public ICollection<EmpresaEntity> ListarEmpresas()
        {
            try
            {

                ICollection<EmpresaEntity> lista = new Collection<EmpresaEntity>();
                EmpresaEntity empresa0 = new EmpresaEntity()
                {
                    Nit = 78945612,
                    Nombre = "IBM",
                    Correo = "soporteibm@ibm.com",
                    FechaFundacion = DateTime.Now.AddYears(-18),
                    Telefono = "3103353295",
                    Direccion = "Cra. 53 # 100 - 25, Suba, Bogotá, Cundinamarca",
                    Estado = true,
                };

                EmpresaEntity empresa1 = new EmpresaEntity()
                {
                    Nit = 963852412,
                    Nombre = "Google",
                    Correo = "soportegoogle@gmail.com",
                    FechaFundacion = DateTime.Now.AddYears(-35),
                    Telefono = "3212323236",
                    Direccion = "Mountain View, CA 94043 Estados Unidos",
                    Estado = true,
                };
                EmpresaEntity empresa2 = new EmpresaEntity()
                {
                    Nit = 74185245,
                    Nombre = "Twitter",
                    Correo = "elonmusk@x.com",
                    FechaFundacion = DateTime.Now.AddYears(-36),
                    Telefono = "3003323635",
                    Direccion = "Trv 71 B N 9 D 90 Marsella Of 503, Bogotá",
                    Estado = true,
                };

                EmpresaEntity empresa3 = new EmpresaEntity()
                {
                    Nit = 456785912,
                    Nombre = "Facebook",
                    Correo = "soportefacebook@facebook.com",
                    FechaFundacion = DateTime.Now.AddYears(-6),
                    Telefono = "3143062025",
                    Direccion = "California",
                    Estado = true,
                };

                lista.Add(empresa0);
                lista.Add(empresa1);
                lista.Add(empresa2);
                lista.Add(empresa3);
                

                return lista;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
