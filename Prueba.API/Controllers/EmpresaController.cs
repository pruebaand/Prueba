﻿using Prueba.Application.UseCases;
using Prueba.Application.UseCases.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController : ControllerBase
    {
        //Creando Objeto de tipo interfaz para poder utilizar las funciones del caso de uso.
        private readonly IEmpresaUseCase _empresaUseCase;

        public EmpresaController()
        {
            _empresaUseCase = new EmpresaUseCase();
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult ListarEmpresas()
        {
            try
            {
                var result = _empresaUseCase.ListarEmpresas();
                if (!result.Succeeded)
                    return BadRequest();
                return new JsonResult(result);
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
